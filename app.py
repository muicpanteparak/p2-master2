from flask import Flask, request, jsonify, send_file
from flask_cors import CORS
from minio import Minio
from minio.error import ResponseError
from os import environ, path
from uuid import uuid4
from random import choices
from string import digits, ascii_lowercase
from pymongo import MongoClient
import requests
from logger import get_logger
logging = get_logger()

MINIO_HOST = environ.get("MY_MINIO_HOST", "localhost")
MINIO_PORT = environ.get("MY_MINIO_PORT", "9000")
MASTER_HOST = environ.get("MY_MASTER_HOST", "http://localhost:5000")
MONGO_HOST = environ.get("MY_MONGO_HOST", "localhost:5672")
DB_NAME = "pdf2text"
COLLECTION_NAME = "jobs"

mc = Minio(f"{MINIO_HOST}:{MINIO_PORT}",
           access_key="admin",
           secret_key='password',
           secure=False)
mongo = MongoClient(host=MONGO_HOST)
db = mongo.get_database(DB_NAME)
jobs = db.get_collection(COLLECTION_NAME)

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['MAX_CONTENT_LENGTH'] = 2048 * 1024 * 1024


def get_job_id():
    return ''.join(choices(ascii_lowercase + digits, k=6))


@app.route('/submitjob', methods=["POST"])
def submit():
    # job_id = request.form['job_id']
    job_id = get_job_id()
    save_path = f"/tmp/{job_id}"
    f = request.files["tarball"]
    f.save(save_path)
    # current_chunk = int(request.form['dzchunkindex'])
    # total_chunks = int(request.form['dztotalchunkcount'])

    # with open(save_path, 'ab') as ff:
    # ff.seek(int(request.form['dzchunkbyteoffset']))
    # ff.write(f.stream.read())

    # if current_chunk + 1 == total_chunks:
    #     # This was the last chunk, the file should be complete and the size we expect
    #     if path.getsize(save_path) != int(request.form['dztotalfilesize']):
    #         return jsonify({"error":"size mismatch"}), 500

    mc.make_bucket(job_id)
    logging.info(f"[ SUBMITTED ] {job_id}")
    # Put object
    object_id = str(uuid4())
    mc.fput_object(job_id, object_id, save_path)
    doc = {
        "file_name": str(f.filename),
        "job_id": str(job_id),
        "object_id": str(object_id)
    }
    jobs.insert_one(doc)
    doc['_id'] = ''
    requests.post(url=f"{MASTER_HOST}/job/create", json=doc)
    logging.info(f"[ QUEUED ] {job_id}")
    return jsonify(doc), 200
    # return jsonify({"msg": "waiting"}), 200


@app.route("/health", methods=["GET", "POST"])
def health():
    return jsonify({"message": "Hello from controller"}), 200


@app.route("/getJob", methods=["POST"])
def get_job():
    data = request.get_json()
    job_id = data["job_id"]
    logging.debug(f"[ JOB CHECK ] {job_id}")
    if jobs.find_one({"job_id": job_id}):
        return jsonify({"found": True}), 200
    return jsonify({"found": False}), 400


@app.route("/getFile/<jid>", methods=["GET"])
def get_file(jid):
    print(f"[ FILE DOWNLOAD ] {jid}")
    job = jobs.find_one({"job_id": jid})
    if not job :
        return jsonify({"status": "Job not found"}), 400
    # object_id = job["object_id"]
    file_name = f"pdf2text-{job['file_name']}"
    outfile = f"/tmp/${jid}/${jid}-final"
    mc.fget_object(jid,f"{jid}-final", outfile)
    return send_file(outfile,attachment_filename=file_name, as_attachment=True), 200


@app.after_request
def creds(response):
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    return response


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9001, debug=False)
