FROM python:3.7
WORKDIR /app
ARG MINIO_URL
ARG REDIS_URL

COPY req.txt req.txt
RUN pip3 install -r req.txt
COPY . .
ENV PYTHONUNBUFFERED TRUE
CMD python3 -u app.py

#CMD gunicorn -w 4 -b 0.0.0.0:9001 app:app

